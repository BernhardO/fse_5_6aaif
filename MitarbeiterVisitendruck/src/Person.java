import java.util.UUID;

public class Person {
    private UUID ID;
    private String name;


    public Person(UUID ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public void setID(UUID ID) {

       if (ID != null){
           setID(ID);
       }
       else
       {
           System.out.println("Nullpoint Exception");
       }
    }

    public void setName(String name) {
        if (name != null){
            setName(name);
        }
        else
        {
            System.out.println("Nullpoint Exception");
        }
    }

    public UUID getID() {

    return ID;
    }

    public String getName() {

           return name;

    }
}
