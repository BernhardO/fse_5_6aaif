import java.util.UUID;

public class Main {
    public static void main(String[] args) {

        //Schilderdruckarten initialisieren
        Namensschilderdruck namensschilderdruck = new Namensschilderdruck();
        Rahmendruck rahmendruck = new Rahmendruck();
        Standarddruck standarddruck = new Standarddruck();

        //Mitarbeiter initialisieren
        Mitarbeiter mitarbeiter = new Mitarbeiter(UUID.randomUUID(), "Gandalf", "gan@gmx.at", "Abteilungsleiter");

        //Visitendruck initialisieren
        Visitenkartendruck visitenkartendruck = new Visitenkartendruck(mitarbeiter, standarddruck);


        //Hier kann man noch den Art des Druckes auswählen (Rahmendruck, Namensschilderdruck)
       // visitenkartendruck.setDrucker();
        visitenkartendruck.drucken();


    }
}
