import java.util.UUID;

public class Mitarbeiter extends Person{

    String email;
    String position;


    public Mitarbeiter(UUID ID, String name, String email, String position) {
        super(ID, name);
        this.email = email;
        this.position = position;
    }

    public void setEmail(String email) {
       if (email != null){
           setEmail(email);
       }else {
           System.out.println("Nullpoint Exception");
       }
    }

    public void setPosition(String position) {
        if (position != null){
            setPosition(position);
        }else
        {
            System.out.println("Nullpoint Excpetion");
        }
    }

    public String getEmail() {
        return email;
    }

    public String getPosition() {
        return position;
    }


}
