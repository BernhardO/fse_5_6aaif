package AT.HTLImst;

import org.apache.commons.collections4.bag.TreeBag;

public class TreeBagBeispiel {
    public static void main(String[] args) {
        TreeBag<String> bag = new TreeBag<>();

        // Hinzufügen von Elementen zur Tasche "Bag"
        bag.add("Mango");
        bag.add("Kiwi");
        bag.add("Mango");

        // Abrufen der Anzahl von Elementen in der Tasche "Bag"
        System.out.println("Anzahl der Mangos: " + bag.getCount("Mango"));

        // Iteration durch die Elemente
        for (String element : bag) {
            System.out.println(element);
        }
    }
}