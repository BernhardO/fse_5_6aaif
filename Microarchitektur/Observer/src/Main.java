public class Main {
    public static void main(String[] args) {

        KollegChat chat = new KollegChat();
        Observer app = new AppChatObserver();


        chat.addObserver(new AppChatObserver());

        chat.setMessage("Hello 1");

        chat.addObserver(new DesktopChatObserver());

        chat.setMessage("Hello 2");

        chat.removeObserver(app);

        chat.setMessage("Hello 2");
    }
}