public class AppChatObserver implements Observer{
    @Override
    public void update(String message) {
        System.out.println("App: " + message);
    }
}
