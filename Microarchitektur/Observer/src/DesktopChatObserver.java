public class DesktopChatObserver implements Observer{

    @Override
    public void update(String message) {
        System.out.println("Desktop: " + message);
    }
}
