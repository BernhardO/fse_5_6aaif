import java.util.ArrayList;
import java.util.List;

public class KollegChat implements Subject{

    private List<Observer> observers;
    private String message;


    public KollegChat() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void addObserver(Observer observer) {

        observers.add(observer);

    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
    for (Observer observer:observers){
        observer.update(message);
    }
    }


    public void setMessage(String message){
        this.message = message;
        notifyObserver();
    }
}
