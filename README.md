# FSE_5_6AAIF


## NR.1 'Dokumentation GIT Befehle' 

Verbinde GitLab mit GIT über SSH -> git clone https://gitlab.com/BernhardO/fse_5_6aaif

Verbinde das Rep. mit VSC -> Clone -> Open in your IDE -> Visual Studio Code HTTPS. Repoziel auswählen

Zum Main Branch navigieren mit cd in Git Konsole. 

git init ->Reinitialized existing Git repository in
git config -> Config Möglichkeiten
git status -> On branch main
Your branch is up to date with 'origin/main'.

git diff -> zeigt die differenz der Änderungen
git branch -> Zeigt die Branches
git rebase-> ändert mehrere commits und passt verlauf des Repos an.
git merge -> mehrere Commits werden einheitlich zusammengeführt um z.b.: 2 Branches zu vereinen. 

## NR.2 'Dokumentation GitHub Flow'

 GitHub-Flow ist ein schlanker, branchbasierter Workflow

 Voraussetzungen: 
 bei Git registrieren
 Repo erstellen 

Erklärung:
Insgesamt ist GitHub Flow eine großartige Wahl für Projekte, bei denen die kontinuierliche Bereitstellung und eine einfache, kollaborative Entwicklungspraxis wichtig sind. Es ist jedoch wichtig zu beachten, dass diese Strategie nicht für alle Arten von Projekten oder Teams geeignet ist, insbesondere nicht für größere und komplexere Projekte. In solchen Fällen kann es notwendig sein, eine andere Branching-Strategie zu wählen, die besser zu den Anforderungen passt.

 ## NR.3  'GitLab Teams' 

# Codeversionierung - Team
*Amar Memagic* <br>
*Bernhard Obermoser* <br>
*Lukas Hutz*



## Experiment 1
Alle Teammitglieder pushen Teile eines gemeinsamen Projektes in die Main-Branch. Als Beispiel wird dafür das Druck-Beispiel aus der ersten Übung verwenden. Zum Schluss sollen die Klassen der einzelnen Entwickler zum gesamten Programm zusammenfügen.

**Aufteilung:** <br>
Amar - Main-Klasse <br>
Bernhard - Druckerklassen <br>
Lukas - Personenklassen <br>

**Ablauf:**
1. Amar: Fügt Main.java zum Projekt hinzu
2. Amar: Spielt Änderung über git auf die Main-Branch
```console
git commit -a -m "Main-Klasse"
git push origin main
```
3. Lukas: Fügt Personenklassen zum Projekt hinzu
4. Lukas: Pullt von der Main-Branch und pusht dann die Personenklassen
```console
git pull origin main
git commit -a -m "Personenklassen"
git push origin main
```
5. Bernhard: Fügt Druckerklassen zum Projekt hinzu
6. Bernhard: Pullt von der Main-Branch und pusht dann die Druckerklassen
```console
git pull origin main
git commit -a -m "Druckerklassen"
git push origin main
```

## Experiment 2
Die Teammitglieder teilen sich in verschiedene Branches auf und aktualisieren ihre Klassen in den jeweiligen Branches. Danach werden alle branches gemergt. 

**Aufteilung:** <br>
Amar - Main-Klasse <br>
Bernhard - Druckerklassen <br>
Lukas - Personenklassen <br>

**Ablauf:**
1. Amar: Erstellt neue Branch
```console
git checkout -b Hauptklassenupdate
```
2. Amar: Aktualisiert Main.java
3. Amar: Pusht Änderungen in Branch Hauptklassenupdate
```console
git commit -a -m "Aktualisierung Main-Klasse"
git push origin Hauptklassenupdate
```
4. Lukas: Erstellt neue Branch
```console
git checkout -b PersonenUpdate
```
5. Lukas: Aktualisiert Personenklassen
6. Lukas: Pusht Änderungen in Branch PersonenUpdate
```console
git commit -a -m "Aktualisierung Druckerklassen"
git push origin PersonenUpdate
```
7. Bernhard: Erstellt neue Branch
```console
git checkout -b Druckerupdate
```
8. Bernhard: Aktualisiert Druckerklassen
9. Bernhard: Pusht Änderungen in Branch Druckerupdate
```console
git commit -a -m "Aktualisierung Druckerklassen"
git push origin Druckerupdate
```
10. Mergen der jeweiligen Branches durch folgende Schritte
```console
git checkout main
git merge BranchName // Ersetzen durch Name der jeweiligen Branch
git pull
git push
```






## NR.4 'Alternativen zu GitHub Flow'

Vier branching Workflows fir Git Systeme. 

# Trunk based Development 
-erklärung: 
Alle Developer machen nur Änderungen in den Main Branch. der Main Branch dient als Trunk, dessen ähnliche Funktionsweise auch bei VLANs zum Einsatz kommt. Wenn eine Funktion jedoch zusätzliche Zeit und Aufmerksamkeit braucht, kann man ihn in einen externen Branch geben, der dann aber wieder in die Main gepusht wird. 

-Vorteile: 
kleine inkrementelle Änderungen, leichtere Übersicht, Continious Integrations ermöglichen kontinuirliches Testen der Software um die Funktionalität sicherzustellen, gute Zusammenarbeit und schnelles Feedback

-Nachteile: 
Konflikte wenn integrationen nicht richtig gehandhabt werden, nicht geiegnet für große komplexe Projekte, "Single Point of failure" wenn die Main Branch fehlerhaft ist.

# Git Flow 
-erklärung:
Es gibt den Dev Branch der von Main entspringt und für die dauernde Entwicklung verantwortlich ist. Wenn man in einem neuen Feature oder Bugfix arbeitet, wird von Develop Branch ein neuer Feature Branch erstellt. Wenn Feature/Bug gefixt sind, wieder in Dev Branch mergen. 

-vorteile: 
Saubere Struktur, zusätzliches von der stabilen version trennen, hohe Collab mit Teammitgliedern, für größere Teams geeignet.


-nachteile:
mögliche merge konflikte, unübersichtliche branches bei komplexen Projekten und Featureerweiterungen, komplexer als andere Branching Strats. 

# GitHub Flow
-erklärung:
GitHub Flow verwendet in der Regel nur einen Main, in der Regel den master-Branch, der als produktive Branch gilt.
Für jede neue Funktion oder jedes Feature erstellt ein Entwickler einen eigenen Feature-Branch. Dieser Branch wird von master abgezweigt.
Entwickler arbeiten in ihren Feature-Branches und führen kontinuierlich Code-Integrationen durch. Sie stellen sicher, dass der Code ständig funktioniert.

-vorteile:
gute zusammenarbeit, einfach verständlich, klare verantwortlichkeiten, gute und leichte Pullrequests für zusammenarbeit.

-nachteile:
Nicht für komplexe und große Teams gedacht 